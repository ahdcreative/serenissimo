import setuptools

with open("readme.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="serebot",
    version="0.0.2",
    author="AHDCreative",
    author_email="luca@ahd-creative.com",
    description="A bot to help people in Veneto to find a spot for getting vaccinated",
    install_requires=[
        "requests>=2,<3",
        "beautifulsoup4>=4,<5",
        "matplotlib>=3,<4",
        "pyTelegramBotAPI>=3,<4",
        "python-dotenv==0.17.0",
    ],
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/ahdcreative/serenissimo",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
        "Operating System :: OS Independent",
    ],
    package_data={"": ["*.sql"]},
    package_dir={"": "."},
    packages=setuptools.find_packages(where="."),
    python_requires=">=3.6",
)
